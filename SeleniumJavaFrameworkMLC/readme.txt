Minimum Requiment

1. You should have Eclipes IDE (Version: 2020-03 (4.15.0)) and Jdk-14 in your machine to run this Automation framework.
2. Please clean and build the project before you run it.
3. Please right click on each "StepDefinition ->xxxxTestRunner.java" file and RunAs TestNG

Question 1 - Web UI test � Part 1

src/test/java
  -> pages - BasePage.java
           - HomePage.java
           - LifeViewPage.java
           - RequestLifeViewDemoPage.java

StepDefinition 

           - RequestDemoMLCTestRunner.java
          
src/test/resources
 -> features - requestDemoMLC.feature


test-output
 -> extentreport - MLCreport.html

------------------------------------

Question 2 - Web UI Test � Part 2 

src/test/java
  -> pages - BasePage.java
           - AtoHome.java
           
StepDefinition 

           - TaxCalculationStepDefinition.java
           
src/test/resources
 -> features - taxCalculation.feature


test-output
 -> extentreport - TaxCalculationReport.html

------------------------------------

Question 3 - API test 

src/test/java
        APIRequests-> APIRequest.java
                      APIResponse.java
           
           
StepDefinition 

           - ParcelShipCostStepDefinition.java
           
src/test/resources
 -> features - GetShipCost.feature


test-output
 -> extentreport - reportAPI.html    

