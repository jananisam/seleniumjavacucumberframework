Feature: MLC Life Insurance
	Scenario Outline: MLC Life Insurance Site Launch and lookup Lifeview
    	Given user launch MLC Insurance page on Chrome Browser
        When user click on Search button
        And enter "Lifeview" in text field
        And click on "LifeView" link  
        And verify breadcrums link "Home","Partnering with us","Superannuation funds"     
        And click "Request a demo" button
        And load the page
        And enter "Name" with <name>
        And enter "Company" with <company>
        And enter "Email" with <email>
        And enter "Phone" with <phone>
        And enter "Preferred contact time" with <time>
        And enter "Request details" with <requesdetails>
        Examples:        
        | name         | company | email           | phone      | time | requesdetails          |
        | Peter Lypsey | Datacom | peter@gmail.com | 0412131415 | AM   | please arrange a demo 1|
        | Sam Tan      | Datacom | sam@gmail.com   | 0416131718 | PM   | please arrange a demo 2|
        | Ben rose     | Quest   | ben@gmail.com   | 0412131899 | AM   | please arrange a demo 3|