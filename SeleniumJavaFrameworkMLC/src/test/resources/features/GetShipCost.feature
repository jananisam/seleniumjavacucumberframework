Feature: Get the shipping cost by passing country code
  Scenario: User calls web service to get shipping cost by passing country code
	Given country list API endpoint verification and store the country code to get service group "28744ed5982391881611cca6cf5c240","1","AL"
        When service list API endpoint verification and store the service code to get calculated price "28744ed5982391881611cca6cf5c240","2.0","0","INT_PARCEL_EXP_OWN_PACKAGING"
        And verify the calculated price of expected service code and weight "28744ed5982391881611cca6cf5c240","2.0","96.55"

  Scenario: User calls web service to get shipping cost by passing country code
	Given country list API endpoint verification and store the country code to get service group "28744ed5982391881611cca6cf5c240","2","DZ"
        When service list API endpoint verification and store the service code to get calculated price "28744ed5982391881611cca6cf5c240","3.0","1","INT_PARCEL_STD_OWN_PACKAGING"
        And verify the calculated price of expected service code and weight "28744ed5982391881611cca6cf5c240","3.0","100.40"

  Scenario: User calls web service to get shipping cost by passing country code
	Given country list API endpoint verification and store the country code to get service group "28744ed5982391881611cca6cf5c240","3","AS"
        When service list API endpoint verification and store the service code to get calculated price "28744ed5982391881611cca6cf5c240","1.0","0","INT_PARCEL_STD_OWN_PACKAGING"
        And verify the calculated price of expected service code and weight "28744ed5982391881611cca6cf5c240","3.0","66.45"

