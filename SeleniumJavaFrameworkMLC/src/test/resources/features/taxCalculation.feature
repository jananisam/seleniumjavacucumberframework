Feature: Simple tax Calculation in ato
	Scenario Outline: Launch ato site and calculate income tax
    	Given user launches tax calculation page on Chrome Browser
        When user should be in "Questions | Simple tax calculator" page
        And fill "IncomeYear" with <incomeyear>
        And fill "TaxableIncome" with <income>
        And fill "ResidencyStatus" with <residentstatus>
        Then click on submit button 
        And verify the income tax value "Result" with <result>
        
        Examples:
        
        | incomeyear   | income  | residentstatus               | result     |
        | 2018-19      | 95000   | Resident for full year       | $22,647.00 |
	| 2017-18      | 150000  | Non-resident for full year   | $51,585.00 | 
        
        
Scenario Outline: Launch ato site and calculate income tax
     Scenario Outline: Launch ato site and calculate income tax
    	Given user launches tax calculation page on Chrome Browser
        When user should be in "Questions | Simple tax calculator" page
        And fill "IncomeYear" with <incomeyear>
        And fill "TaxableIncome" with <income>
        And fill "ResidencyStatus" with <residentstatus>
	And fill "NoOfMonths" with <noofmonths>
        Then click on submit button 
        And verify the income tax value "Result" with <result>
          
     Examples: 
       
        | incomeyear   | income  | residentstatus      | noofmonths    | result      |
        | 2016-17      | 110000  | Part-year resident  | 1             | $29,156.79  |
