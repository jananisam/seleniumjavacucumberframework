package stepTaxCalculationDefinition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AtoHomePage;

public class TaxCalculationStepDefinition {
	private WebDriver driver;
		
	@Given("^user launches tax calculation page on Chrome Browser$")
	public void user_launches_tax_calculation_page_on_Chrome_Browser() throws Throwable { 	    
		
    	System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver\\chromedriver.exe");   	
    	
    	driver = new ChromeDriver();
		driver.get("https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions");
		driver.manage().window().maximize();
	}

	@When("^user should be in \"([^\"]*)\" page$")
    public void user_should_be_in_page(String pageTitle) throws Throwable {
		AtoHomePage homePg = new AtoHomePage(driver);
		homePg.verifyPageTitle(pageTitle);		
	}
	
	@And("^fill \"([^\"]*)\" with (.+)$")
	public void enterValue(String field, String value) throws InterruptedException {
		AtoHomePage homePg = new AtoHomePage(driver);
		homePg.fillTheForm(field,value);
	}
	
	 @Then("click on submit button")
	 public void click_on_submit_button() throws Throwable {
		 AtoHomePage homePg = new AtoHomePage(driver);
		 homePg.submitForm();
	    }
	
	@And("^verify the income tax value \\\"([^\\\"]*)\\\" with (.+)$")
	public void veirfyResult(String field, String value) throws InterruptedException {
		AtoHomePage homePg = new AtoHomePage(driver);
		homePg.verifyResult(field,value);
	}
	
	@After
	 public void tearDown() {
		driver.quit(); 
	}

	
}
