package stepRequestDemoDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LifeViewPage;
import pages.RequestLifeViewDemoPage;

public class RequestDemoMLCStepDefinition {	
	private WebDriver driver;
	

	
	@Given("^user launch MLC Insurance page on Chrome Browser$")
	public void user_launch_a_page_URL() throws Throwable {    	
		System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver\\chromedriver.exe");   	

		driver = new ChromeDriver();
		driver.get("https://www.mlcinsurance.com.au/");
		driver.manage().window().maximize();		
	}    

	@When("^user click on Search button$")
	public void i_go_to_Search_popup() { 
		HomePage homePage = new HomePage(driver);
		homePage.clickOnSearchPopup();
	}

	@And("^enter \"([^\"]*)\" in text field$")
	public void click_on_search_button(String input) throws Throwable {
		HomePage homePage = new HomePage(driver);
		homePage.searchTheText(input);
	}

	@Then("^click on \"([^\"]*)\" link$")
	public void click_on_link(String linkText) throws Exception {
		HomePage homePage = new HomePage(driver);
		homePage.clickOnLink(linkText);
	}
  
	@And("^verify breadcrums link \\\"([^\\\"]*)\\\",\\\"([^\\\"]*)\\\",\\\"([^\\\"]*)\\\"$")
	public void verify_breadcrums_link(String link1,String link2, String link3) throws InterruptedException{
		LifeViewPage lifeViewPg = new LifeViewPage(driver);
		lifeViewPg.verifyBreadCrumb(link1,link2,link3);
	}

	@And("^click \"([^\"]*)\" button$")
	public void click_on_button(String bntName) throws Exception {
		LifeViewPage lifeViewPg = new LifeViewPage(driver);
		lifeViewPg.clickOnRequestDemo(bntName);
	}

	@And("^load the page$")
	public void loadPage() throws InterruptedException {
		RequestLifeViewDemoPage lifeViewDemoPg = new RequestLifeViewDemoPage(driver);
		lifeViewDemoPg.loadThePage();	
	}

	@And("^enter \"([^\"]*)\" with (.+)$")
	public void enterValue(String field, String value) throws InterruptedException {
		RequestLifeViewDemoPage lifeViewDemoPg = new RequestLifeViewDemoPage(driver);
		lifeViewDemoPg.fillTheForm(field,value);
	}
	
	@After
	 public void closeDriver() {
		driver.quit(); 
	}



}