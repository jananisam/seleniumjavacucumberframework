package stepdefinitions;

import org.testng.Assert;

import APIRequests.APIResponse;
import FileMain.JsonPathFile;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.ValidatableResponse;
import responseValueExtracter.ResponseValidator;

public class ParcelShipCostStepDefinitions  {
	
    APIResponse apiResponse = new APIResponse();
    
    @Given("country list API endpoint verification and store the country code to get service group {string},{string},{string}")
	public void shipping_country_list(String apiToken,String index,String expectedResult) throws ClassNotFoundException, InterruptedException{
    	
    	ValidatableResponse response = apiResponse.countryList(
    			apiToken,index
    		);

    		ResponseValidator validator = new ResponseValidator();
    		validator.verifyResponseCode(response, 200);
    		
    		
    		Assert.assertTrue(validator.verifySingleStringNumbericValue(response,JsonPathFile.getJsonPath("contryList"),expectedResult));
    		System.out.println(">>>>>>>>>>>>>Verified the expected country code visibility in retrived country code list");
    		
	
	}
    
    @When("service list API endpoint verification and store the service code to get calculated price {string},{string},{string},{string}")
   	public void shipping_service_code(String apiToken,String weight,String index,String expectedResult) throws ClassNotFoundException, InterruptedException{
       	
       	ValidatableResponse response = apiResponse.serviceList(apiToken,weight,index);

       		ResponseValidator validator = new ResponseValidator();
       		validator.verifyResponseCode(response, 200);
       		
       		
       		Assert.assertTrue(validator.verifySingleStringNumbericValue(response,JsonPathFile.getJsonPath("serviceList"),expectedResult));
       		System.out.println(">>>>>>>>>>>>>Verified the expected country code visibility in retrived country code list");
   	}
    
    @And("verify the calculated price of expected service code and weight {string},{string},{string}")
   	public void shipping_calculated_service_cost(String apiToken,String weight,String expectedResult) throws ClassNotFoundException, InterruptedException{
       	
       	ValidatableResponse response = apiResponse.calculatedPrice(apiToken,weight);

       		ResponseValidator validator = new ResponseValidator();
       		validator.verifyResponseCode(response, 200);
       		
       		
       		Assert.assertTrue(validator.verifySingleStringNumbericValue(response,JsonPathFile.getJsonPath("calculatedCost"),expectedResult));
       		System.out.println(">>>>>>>>>>>>>Verified the expected final cost of expected shipping service and expected weight");
   	}
	
}