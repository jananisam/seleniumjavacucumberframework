package APIRequests;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import FileMain.DataKey;
import FileMain.FileReadWriterCommon;
import FileMain.JsonPathFile;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
public class APIRequest {
	
	FileReadWriterCommon propFile;
	private ValidatableResponse validatableResponse;
	private RequestSpecification request;
	private String ENDPOINT_GET_COUNTRY_LIST = "https://digitalapi.auspost.com.au/postage/country.json";
	private String ENDPOINT_GET_SERVICE_CODE = "https://digitalapi.auspost.com.au/postage/parcel/international/service.json";
	private String ENDPOINT_GET_CALCULATED_COST = "https://digitalapi.auspost.com.au/postage/parcel/international/calculate.json";
	
	
	public void setPropertyFile(FileReadWriterCommon file) {
		propFile = file;
	}

    public String extractSingleValueFromResponse(ValidatableResponse response, String jsonPath)
	{
		System.out.println("EXTRACTING SINGLE VALUE FROM RESPONSE ON " + jsonPath);
		try	{
			return response.extract().path(jsonPath).toString();			
		}		
		catch (Exception e)
		{
			System.out.println("Error returning value for the jsonPath " + jsonPath);
		    return "";
		}
	}
    
	public ValidatableResponse getTheCountryList(String apiToken,String index) {
		System.out.println("GET THE COUNTRY LIST REQUEST");

		try {
			request = given()
					  .headers("AUTH-KEY",apiToken);
			
			validatableResponse = request.get(ENDPOINT_GET_COUNTRY_LIST).then();
			System.out.println(validatableResponse.extract().asString());
			
			System.out.println(extractSingleValueFromResponse(validatableResponse,JsonPathFile.getJsonPath("contryList")));
			String str[] = extractSingleValueFromResponse(validatableResponse,JsonPathFile.getJsonPath("contryList")).split(",");
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(str);
			System.out.println(al.get(Integer.parseInt(index)).trim());
			
			propFile.saveProperty(DataKey.COUNTRYCODE, al.get(Integer.parseInt(index)).trim());
			return validatableResponse;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public ValidatableResponse getPostageServicesList(String apiToken,String weight,String index) {
		System.out.println("GET THE POSTAGE SERVICE LIST REQUEST");

		try {
			request = given()
					  .headers("AUTH-KEY",apiToken)
					  .queryParam("country_code", propFile.getPropertyValues(DataKey.COUNTRYCODE))
					  .queryParam("weight", weight);
			
			validatableResponse = request.get(ENDPOINT_GET_SERVICE_CODE).then();
			System.out.println(validatableResponse.extract().asString());
			
			System.out.println(extractSingleValueFromResponse(validatableResponse,JsonPathFile.getJsonPath("serviceList")));
			String str[] = extractSingleValueFromResponse(validatableResponse,JsonPathFile.getJsonPath("serviceList")).split(",");
			List<String> al = new ArrayList<String>();
			al = Arrays.asList(str);
			System.out.println(al.get(Integer.parseInt(index)).trim());
			
			propFile.saveProperty(DataKey.SERVICECODE,al.get(Integer.parseInt(index)).trim().replace("[", ""));
			return validatableResponse;
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		}
	}

	public ValidatableResponse getCalculatedShippingCost(String apiToken,String weight) {
		System.out.println("GET THE SHIPPING COST FOR PARTICULAR SERVICE REQUEST");

		try {
			request = given()
					  .headers("AUTH-KEY",apiToken)
					  .queryParam("country_code", propFile.getPropertyValues(DataKey.COUNTRYCODE))
					  .queryParam("weight", weight)
					  .queryParam("service_code", propFile.getPropertyValues(DataKey.SERVICECODE));
			
			validatableResponse = request.get(ENDPOINT_GET_CALCULATED_COST).then();
			System.out.println(validatableResponse.extract().asString());
			
			System.out.println(extractSingleValueFromResponse(validatableResponse,JsonPathFile.getJsonPath("calculatedCost")));
			return validatableResponse;
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		}
	}
}
