package APIRequests;

import FileMain.FileReadWriterCommon;
import io.restassured.response.ValidatableResponse;
public class APIResponse {
		
	APIRequest apiRequest = new APIRequest();
	FileReadWriterCommon propfile = new FileReadWriterCommon();
	
	public APIResponse() {
		propfile.createPropertyFile("Ship");
		apiRequest.setPropertyFile(propfile);
	}
	
	  //Country List
	  public ValidatableResponse countryList (String apiToken,String index) throws InterruptedException
	  {
			Thread.sleep(2000);//this is mandatory
			ValidatableResponse response = apiRequest.getTheCountryList(apiToken,index);
					
			return response;
	  }
	  
	//Services List
	  public ValidatableResponse serviceList (String apiToken,String weight,String index) throws InterruptedException
	  {
			Thread.sleep(2000);//this is mandatory
			ValidatableResponse response = apiRequest.getPostageServicesList(apiToken,weight,index);
					
			return response;
	  }
	  
	  
	//Get Calculated price
	  public ValidatableResponse calculatedPrice (String apiToken,String wight) throws InterruptedException
	  {
			Thread.sleep(2000);//this is mandatory
			ValidatableResponse response = apiRequest.getCalculatedShippingCost(apiToken,wight);
					
			return response;
	  }
}