package test;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.And;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.gherkin.model.Then;
import com.aventstack.extentreports.gherkin.model.When;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/taxCalculation.feature", glue = {"stepTaxCalculationDefinition"}, tags = {"~@Ignore"}, 
plugin = { "pretty", "html:HTML-Reports"} )
public class TaxCalculationTestRunner extends AbstractTestNGCucumberTests {
	
	private TestNGCucumberRunner testNGCucumberRunner;

	private ExtentHtmlReporter htmlReporter;
	private ExtentReports extentReport;	

	@BeforeClass
	public void setUp() {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		
		setupExtentHTMLReporter();
		
		// feature
		ExtentTest feature = extentReport.createTest(Feature.class, "Tax Calculator");

		// scenario
		ExtentTest scenario = feature.createNode(Scenario.class, "Simple tax Calculation in ato");

		// steps
		scenario.createNode(Given.class, "user launches tax calculation page on Chrome Browser").pass("pass");
		scenario.createNode(When.class, "user should be in \"Simple tax calculator\" page").pass("pass");		
		scenario.createNode(And.class, "Fill the form").pass("pass");
		scenario.createNode(Then.class, "click on submit button").pass("pass");
		scenario.createNode(And.class, "verify the income tax <result>").pass("pass");
		
	}	

	@AfterClass
	public void tearDown() {		
		extentReport.flush();
	}	
	
	private void setupExtentHTMLReporter() {
		htmlReporter = new ExtentHtmlReporter("test-output\\extentreport\\TaxCalculationreport.html");

		//create ExtentReports and attach reporter
		extentReport = new ExtentReports();
		extentReport.attachReporter(htmlReporter);

		htmlReporter.config().setCSS("css-string");
		htmlReporter.config().setDocumentTitle("page title");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setJS("js-string");
		htmlReporter.config().setProtocol(Protocol.HTTPS);
		htmlReporter.config().setReportName("build name");
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");		
	}	
}