package test;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.And;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.gherkin.model.When;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/requestDemoMLC.feature", glue = {"stepRequestDemoDefinitions"}, tags = {"~@Ignore"}, 
plugin = { "pretty", "html:HTML-Reports"} )
public class RequestDemoMLCTestRunner extends AbstractTestNGCucumberTests {
	
	private TestNGCucumberRunner testNGCucumberRunner;

	private ExtentHtmlReporter htmlReporter;
	private ExtentReports extentReport;	

	@BeforeClass
	public void setUp() {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		
		setupExtentHTMLReporter();
		
		// feature
		ExtentTest feature = extentReport.createTest(Feature.class, "MLC Life Insurance");

		// scenario
		ExtentTest scenario = feature.createNode(Scenario.class, "MLC Life Insurance Site Launch and lookup Lifeview");

		// steps
		scenario.createNode(Given.class, "user launch MLC Insurance page on Chrome Browser").pass("pass");
		scenario.createNode(When.class, "user click on Search button").pass("pass");
		scenario.createNode(And.class, "enter \"Lifeview\" in text field").pass("pass");		
		scenario.createNode(And.class, "click on \"Lifeview\" link ").pass("pass");
		scenario.createNode(And.class, "verify breadcrums link \"Home\",\"Partnering with us\",\"Superannuation funds\"").pass("pass");
		scenario.createNode(And.class, "click \"Reuest a Demo\" button ").pass("pass");
		scenario.createNode(And.class, "load the \"Request a LifeView demo\" page").pass("pass");
		scenario.createNode(And.class, "Enter the details").pass("pass");
		
	}	

	@AfterClass
	public void tearDown() {		
		extentReport.flush();
	}	
	
	private void setupExtentHTMLReporter() {
		htmlReporter = new ExtentHtmlReporter("test-output\\extentreport\\MLCreport.html");

		//create ExtentReports and attach reporter
		extentReport = new ExtentReports();
		extentReport.attachReporter(htmlReporter);

		htmlReporter.config().setCSS("css-string");
		htmlReporter.config().setDocumentTitle("page title");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setJS("js-string");
		htmlReporter.config().setProtocol(Protocol.HTTPS);
		htmlReporter.config().setReportName("build name");
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");		
	}	
	
	
}