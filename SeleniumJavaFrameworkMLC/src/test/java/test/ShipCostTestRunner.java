package test;

import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.And;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.gherkin.model.When;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/GetShipCost.feature", glue = {"stepdefinitions"}, tags = {"~@Ignore"}, 
	plugin = { "pretty", "html:HTML-Reports"} )
public class ShipCostTestRunner extends AbstractTestNGCucumberTests {
	
	private TestNGCucumberRunner testNGCucumberRunner;

	private ExtentHtmlReporter htmlReporter;
	private ExtentReports extentReport;	
	
	@BeforeClass
	public void setUp() {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		
		setupExtentHTMLReporter();
		
		// feature
		ExtentTest feature = extentReport.createTest(Feature.class, "Get the shipping cost by passing country code");

		// scenario
		ExtentTest scenario = feature.createNode(Scenario.class, "User calls web service to get shipping cost by passing country code");

		// steps
		scenario.createNode(Given.class, "country list API endpoint verification and store the country code to get service group").pass("pass");
		scenario.createNode(When.class, "service list API endpoint verification and store the service code to get calculated price").pass("pass");	
		scenario.createNode(And.class, "verify the calculated price of expected service code and weight").pass("pass");		
	}	

	@AfterClass
	public void tearDown() {		
		extentReport.flush();
	}	
	
	private void setupExtentHTMLReporter() {
		htmlReporter = new ExtentHtmlReporter("test-output\\extentreport\\reportAPI.html");

		//create ExtentReports and attach reporter
		extentReport = new ExtentReports();
		extentReport.attachReporter(htmlReporter);

		htmlReporter.config().setCSS("css-string");
		htmlReporter.config().setDocumentTitle("page title");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setJS("js-string");
		htmlReporter.config().setProtocol(Protocol.HTTPS);
		htmlReporter.config().setReportName("build name");
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss");		
	}	
}