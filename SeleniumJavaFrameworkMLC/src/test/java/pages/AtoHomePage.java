package pages;

import static customLocators.enums.How.XPATH;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import customLocators.annotations.SearchBy;
import customLocators.utils.DynamicElement;



public class AtoHomePage extends BasePage {

	public AtoHomePage(WebDriver driver) {
		super(driver);
		
	}

	/*Page Objects*/
	
	@FindBy(how = How.XPATH, using = "//select[@id='ddl-financialYear']")
	public WebElement dropDown_incomeYear;
	
	@FindBy(how = How.NAME, using = "texttaxIncomeAmt")
	public WebElement txt_taxableIncome;

	@SearchBy(searchBy=XPATH, value="//span[text()='?']/..")
	public DynamicElement rdBtn_residencyStatus;
	
	@FindBy(how = How.XPATH, using = "//select[@id='ddl-residentPartYearMonths']")
	public WebElement dropDown_partYearMonths;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Submit']")
	public WebElement btn_submit;
	
	@FindBy(how = How.XPATH, using = "//h2[text()='Result']/..//p//span")
	public WebElement txt_result;
	
	
	
	
	/*Page Objects Methods*/

	public void verifyPageTitle(String pgTitle) {
		Assert.assertEquals(driver.getTitle(),pgTitle);
	}
	
	public void fillTheForm(String field,String value) throws InterruptedException {
		if (field.equals("IncomeYear")) {
			Select sltIncomeYear = new Select(dropDown_incomeYear);
			sltIncomeYear.selectByVisibleText(value);
		} else if (field.equals("TaxableIncome")){
			txt_taxableIncome.sendKeys(value);
		} else if (field.equals("ResidencyStatus")){
			click(updateElement(rdBtn_residencyStatus,value));
			Thread.sleep(5000);
		}else if(field.equals("NoOfMonths")) {
			Select yrMonth = new Select(dropDown_partYearMonths);
			yrMonth.selectByVisibleText(value);	
		}

		
	}
	
	public void submitForm() throws InterruptedException {
		btn_submit.click();
		Thread.sleep(5000);
	}
	
	public void verifyResult(String field,String value) {
		if (field.equals("Result")) {
			Assert.assertEquals(txt_result.getText(),value);
		}else {
			System.out.print("No Result");
		}
		
	}
}
