package pages;

import static customLocators.enums.How.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import customLocators.annotations.SearchBy;
import customLocators.utils.DynamicElement;



public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
		
	}

	/*Page Objects*/
	
	@FindBy(how = How.XPATH, using = "//button[@data-popover-id='global-search']")
	public WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//input[@type='search']")
	public WebElement searchField;
	
	@SearchBy(searchBy=XPATH, value="//div[@id='q-autocomplete-results']//strong[text()='?']/ancestor::a")
	public DynamicElement link_searchOption;
	
	
	/*Page Objects Methods*/
	public void clickOnSearchPopup() {
		searchButton.click();
	}
	
	public void searchTheText(String searchTxt) {
		searchField.sendKeys(searchTxt);
	}
	
	public void clickOnLink(String searchTxt) throws InterruptedException {
		Thread.sleep(5000);
		click(updateElement(link_searchOption,searchTxt));
	}

}
