package pages;

import static customLocators.enums.How.XPATH;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import customLocators.annotations.SearchBy;
import customLocators.utils.DynamicElement;



public class RequestLifeViewDemoPage extends BasePage {

	public RequestLifeViewDemoPage(WebDriver driver) {
		super(driver);
		
	}

	/*Page Objects*/
	
	@SearchBy(searchBy=XPATH, value="//h1[text()='?']")
	public DynamicElement txt_pageTitle;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Name']//following-sibling::input")
	public WebElement txt_name;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Company']//following-sibling::input")
	public WebElement txt_company;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Email']//following-sibling::input")
	public WebElement txt_email;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Phone']//following-sibling::input")
	public WebElement txt_phone;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Preferred contact date']//following-sibling::input")
	public WebElement txt_date;
	
	@SearchBy(searchBy=XPATH, value="//input[@value='?']")
	public DynamicElement txt_time;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Request details']//following-sibling::textarea")
	public WebElement txt_details;
	
	
	/*Page Objects Methods*/
	public void loadThePage() throws InterruptedException {
		//Wait till page completely load
	    ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
		
	}
	
	public void fillTheForm(String field,String value) throws InterruptedException {
		if (field.equals("Company")) {
			txt_company.sendKeys(value);
		} else if (field.equals("Name")){
			txt_name.sendKeys(value);
		} else if (field.equals("Email")){
			txt_email.sendKeys(value);
		} else if (field.equals("Phone")){
			txt_phone.sendKeys(value);
		} else if (field.equals("Preferred contact time")){
			click(txt_time.updateElement(value));
		} else if (field.equals("Request details")) {
			txt_details.sendKeys(value);
		}
	}
	
	
}
