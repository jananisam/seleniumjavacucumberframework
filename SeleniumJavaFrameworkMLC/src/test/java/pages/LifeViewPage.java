package pages;

import java.util.Arrays;
import java.util.List;


import static customLocators.enums.How.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import customLocators.annotations.SearchBy;
import customLocators.utils.DynamicElement;



public class LifeViewPage extends BasePage {

	public LifeViewPage(WebDriver driver) {
		super(driver);
		
	}

	/*Page Objects*/
	
	@FindBy(how = How.XPATH, using = "//ul[@itemprop='breadcrumb']//a")
	public List<WebElement> breadCrumbLink;
	
	@SearchBy(searchBy=XPATH, value="//span[text()='?']/ancestor::a")
	public DynamicElement link_demo;
	
	
	/*Page Objects Methods*/
	public void verifyBreadCrumb(String link1, String link2, String link3) throws InterruptedException {
		Thread.sleep(5000);
		
		List<String> expected = Arrays.asList(link1, link2, link3);
    	
    	for (int i = 0; i < expected.size(); i++) {
    	    String breadcrumb = breadCrumbLink.get(i).getText();
    	    //Without Assertion validating
    	    if (breadcrumb.equals(expected.get(i))) {
    	        System.out.println("passed on: " + breadcrumb);
    	    } else {
    	        System.out.println("failed on: " + breadcrumb);
    	    }
    	    
    	    //Validating through assertion TestNG
    	    Assert.assertEquals(breadcrumb, expected.get(i));

    	}
	}
	
	
	public void clickOnRequestDemo(String demoName) throws InterruptedException {
		Thread.sleep(5000);
		click(updateElement(link_demo,demoName));
	}
	
	
}
