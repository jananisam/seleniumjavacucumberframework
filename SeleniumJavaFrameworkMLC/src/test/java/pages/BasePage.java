package pages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import customLocators.annotations.SearchBy;
import customLocators.utils.DynamicElement;

public class BasePage {
	public  WebDriver driver;
	 
	public BasePage(WebDriver driver) { 	 
		this.driver = driver;	 
		PageFactory.initElements(driver, this); 
		initElements();
	 
	}
	
	
	public final void initElements() {

        final List<Field> fields = new ArrayList<Field>();
        Class currentPageObject = this.getClass();

        while (currentPageObject != BasePage.class) {
            fields.addAll(new ArrayList<Field>(Arrays.asList(currentPageObject.getDeclaredFields())));
            currentPageObject = currentPageObject.getSuperclass();
        }

        for (Field field : fields) {
            final SearchBy fieldAnnotation = field.getAnnotation(SearchBy.class);
            final boolean accessible = field.isAccessible();

            if (fieldAnnotation != null) {
                try {
                    field.setAccessible(true);
                    field.set(this, new DynamicElement(fieldAnnotation.searchBy(), fieldAnnotation.value()));
                    field.setAccessible(accessible);
                } catch (IllegalAccessException e) {
                    // Log or throw your exception here
                }
            }
        }
    }

	/**
	 * Updating the values of dynamic web elements.
	 * @param element DynamicElement
	 * 
	 */
	public DynamicElement updateElement(DynamicElement element, String... values) {
		return element.updateElement(values);
	}

	/**
	 * Find the first matching dynamic element on the current page
	 * @param element Dynamic Element
	 * 
	 */
	public WebElement findElement(DynamicElement element) {
		return driver.findElement(element.getLocator());
	}

	/**
	 * Click this dynamic element
	 * @param element Dynamic Element
	 * 
	 */
	public void click(DynamicElement element) {
		findElement(element).click();
	}

	/**
	 * Get the innerText of this element..
	 * @param element Dynamic Element
	 * 
	 */
	public String getText(DynamicElement element) {
		return findElement(element).getText();
	}

	/**
	 * A list of all dynamic elements, or an empty list if nothing matches.
	 * @param element Dynamic Element
	 * 
	 */
	public  List<WebElement> findElements(DynamicElement element) {
		return findElement(element).findElements(element.getLocator());
	}

	/**
	 * Clear the value
	 * @param element Dynamic Element
	 * 
	 */
	public void clear(DynamicElement element) {
		findElement(element).clear();
	}

	
}
