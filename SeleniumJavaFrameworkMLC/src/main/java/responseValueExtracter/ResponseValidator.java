package responseValueExtracter;

import io.restassured.response.ValidatableResponse;
import static org.hamcrest.Matchers.*;

public class ResponseValidator {
	
	public void verifyResponseCode(ValidatableResponse response, int responseCode) {
		System.out.println("VERIFY RESPONSE CODE " + responseCode);
		try {
			response.assertThat().statusCode(responseCode);
		} catch (Exception e) {
			System.out.println("Status code expected was incorrect! " + e.getMessage());
		}
	}
	
	public boolean verifySingleStringNumbericValue(ValidatableResponse response, String path, String expectedValue) {
		System.out.println("VERIFY SINGLE STRING AND NUMERIC VALUE FROM LIST/ARRAY RESPONSE " + expectedValue);
		
		try {
			String  value = new ResponseValueExtractor().extractSingleValueFromResponse(response, path);
					return value.contains(expectedValue.toString());
		}
		catch (Exception e) {
			System.out.println("String value expected was incorrect! " + e.getMessage());
			return false;
		}
		
	}
}
