package responseValueExtracter;

import com.jayway.jsonpath.Configuration;


import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;


import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;


class ResponseValueExtractor {
	
	public String extractSingleValueFromResponse(ValidatableResponse response, String jsonPath)
	{
		System.out.println("EXTRACTING SINGLE VALUE FROM RESPONSE ON " + jsonPath);
		try
		{
			return response.extract().path(jsonPath).toString();			
		}
		
		catch (Exception e)
		{
			System.out.println("Error returning value for the jsonPath " + jsonPath);
		     return "";
		}
	}
	
	public String verifySingleStringValueFromTextResponse(Response response, String path) 
	{
		System.out.println("EXTRACT SINGLE STRING VALUE FROM TEXT RESPONSE ");
		try {
			return response.jsonPath().get(path).toString();
		} catch (Exception e) {
			System.out.println("Error extracting JSON path from the response");
			return "0";
		}
	}
	
	/**
	 * Get the the value by giving the JSON path
	 * @param jsonResponse The response query which should be in json
	 * @param jsonPath Mention the json path from where you need to get the value
	 */
    public String getJsonPath(String jsonResponse,String jsonPath){
    	 Configuration conf=Configuration.defaultConfiguration();
		return (JsonPath.using(conf).parse(jsonResponse).read(jsonPath)).toString();

    }
}
