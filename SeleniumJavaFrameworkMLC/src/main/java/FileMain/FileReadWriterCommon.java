package FileMain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReadWriterCommon {
	
	String dynaPropFilePath;
	//String shippingTestDataPath = "./src/test/resources/data/TestData.properties";
	static String jsonDataPath = "./src/test/resources/data/JsonPaths.properties";
	
	 static Properties jsonProp = new Properties();
	static boolean propLoaded = false;
	
//	public String shippingTestDataPath()
//	{
//		return shippingTestDataPath;
//	}
//	
	
	
	public void createPropertyFile(String fileName)
	{
		dynaPropFilePath = "./src/test/resources/Data/" + fileName + ".properties";
		
		File file = new File(dynaPropFilePath);
		
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // if file already exists will do nothing
	}

	public void saveProperty(DataKey key, String value) {
		Properties prop = new Properties();

		try {

			FileInputStream input = new FileInputStream(dynaPropFilePath);
			// load existing property values
			prop.load(input);

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// set the properties value
			prop.setProperty(key.toString(), value);

			FileOutputStream output = new FileOutputStream(dynaPropFilePath);
			// save properties to project root folder
			prop.store(output, null);

			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} catch (IOException io) {
			io.printStackTrace();
		}

	}

	public String getPropertyValues(DataKey key) {
		Properties prop = new Properties();

		try {

			FileInputStream input = new FileInputStream(dynaPropFilePath);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			String value = prop.getProperty(key.toString());

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return value;

		} catch (IOException ex) {
			ex.printStackTrace();
			return "";
		}

	}
	
	public static String getPropertyValues(String key) {

		try {

			FileInputStream input = null;

			// load a properties file
			if(propLoaded == false)
			{
				input = new FileInputStream(jsonDataPath);
				jsonProp.load(input);
				propLoaded = true;
			}

			// get the property value and print it out
			String value = jsonProp.getProperty(key);

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return value;

		} catch (IOException ex) {
			ex.printStackTrace();
			return "";
		}

	}

}
